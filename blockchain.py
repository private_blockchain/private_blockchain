import json
import sqlite3 as sl
from hashlib import sha256
from Crypto.PublicKey import RSA
import threading
import socket
import time

class Block:
    def __init__(self, index, data, previous_hash):
        self.index = index
        self.data = data
        self.previous_hash = previous_hash

    def compute_hash(self):
        block_string = json.dumps(self.__dict__)
        return sha256(block_string.encode()).hexdigest()

class Blockchain:
    def __init__(self):
        self.chain = []

    def create_genesis_block(self):
        self.chain = []
        genesis_block = Block(0, '', '0')
        genesis_block.hash = genesis_block.compute_hash()
        self.chain.append(genesis_block)

    def last_block(self):
        return self.chain[-1]

    def add_block(self, block):
        previous_hash = self.last_block().hash

        if previous_hash != block.previous_hash:
            return False

        block.hash = block.compute_hash()
        self.chain.append(block)

        return True

    def from_json(self, str):
        try:
            j = json.loads(str)
            jblocks = j['blocks']

            self.create_genesis_block()

            for i in range(1, len(jblocks)):
                if (jblocks[i-1]['hash'] != self.last_block().hash or
                        jblocks[i]['previous_hash'] != self.last_block().hash):
                            raise Exception
                else:
                    block = Block(i, jblocks[i]['data'], self.last_block().hash)
                    self.add_block(block)

            if jblocks[len(jblocks)-1]['hash'] != self.last_block().hash: raise Exception

        except:
            self.chain = []
            return False

        return True

    def to_json(self):
        blocks = {'blocks':[]}
        for block in self.chain:
            blocks['blocks'].append({'index':block.index,'data':block.data,'previous_hash':block.previous_hash,'hash':block.hash})

        return json.dumps(blocks)

class Verifier:
    def __init__(self):
        print('Verifier')

    def verify(self):
        return False

class Authority:
    def __init__(self):
        print('Authority')

class Group1:
    def __init__(self):
        print('Group1')

class Group2:
    def __init__(self):
        print('Group2')

class Application:
    def __init__(self):
        self.print_info()

        if self.connect_db() != True:
            print('Ошибка базы данных!')
            exit(1)

        exists = self.select('Создать пользователя?:\n[1] Да\n[2] Выбрать существующего\n>>> ', [1, 2])

        if (exists == 2):
            while True:
                try:
                    self.id = self.select('Введите id пользователя:\n>>> ', [], True)
                    self.curs.execute('select * from users where id = ' + str(self.id))
                    data = self.curs.fetchone()
                    self.role = int(data[1])
                    self.publickey = data[2]
                    self.privatekey = data[3]
                except Exception:
                    continue
                break
        else:
            self.role = self.select('Выберите роль:\n[1] Группа 1\n[2] Группа 2\n[3] Авторизованное лицо\n>>> ', [1, 2, 3])
            key = RSA.generate(1024)
            self.publickey = key.publickey().exportKey('DER').hex()
            self.privatekey = key.exportKey('DER').hex()
            self.curs.execute('insert into users (role, publickey, privatekey) values (?, ?, ?)', (str(self.role), self.publickey, self.privatekey))
            self.db.commit()
            self.curs.execute('select id from users where publickey = ?', (self.publickey, ))
            data = self.curs.fetchone()
            self.id = int(data[0])

        if self.connect_network() != True:
            print('Отсутствует подключение к сети!')
            exit(2)

        self.blockchain = Blockchain()
        try:
            f = open('blockchains/'+str(self.id)+'.txt', 'r')
            if not self.blockchain.from_json(f.read()):
                f.close()
                raise Exception
        except:
            f = open('blockchains/'+str(self.id)+'.txt', 'w+')
            self.blockchain.create_genesis_block()
            f.write(self.blockchain.to_json())
            f.close()

        # depends on the protocol
        #if role == 1:
        #    group1 = Group1()
        #elif role == 2:
        #    group2 = Group2()
        #elif role == 3:
        #    authority = Authority()

        threading.Thread(target=self.handler).start()
        time.sleep(1)
        self.sendmsg(self.s, {'code': 'req_blockchain'})
        time.sleep(2)

        while True:
            data = input('Введите данные для создания нового блока:\n')
            new_block = Block(self.blockchain.last_block().index + 1, data, self.blockchain.last_block().hash)
            self.blockchain.add_block(new_block)
            self.save_blockchain_to_file()
            self.sendmsg(self.s, {'code':'new_block', 'block': new_block.__dict__})
            print('Блок отправлен')
            time.sleep(2)

    def handler(self):
        while True:
            try:
                msg = self.recvmsg(self.s)

                if msg['code'] == 'new_block':
                    print('Получен новый блок')
                    new_block = Block(self.blockchain.last_block().index + 1, msg['block']['data'], msg['block']['previous_hash'])
                    if not self.blockchain.add_block(new_block):
                        print('Конфликт блокчейна!')
                        self.sendmsg(self.s, {'code': 'req_blockchain'})
                    else:
                        self.save_blockchain_to_file()

                elif msg['code'] == 'res_blockchain':
                    print(msg)
                    print('Выполняется консенсус')
                    for jblockchain in msg['blockchains']:
                        vs_blockchain = Blockchain()
                        if not vs_blockchain.from_json(json.dumps(jblockchain)):
                            continue
                        elif len(self.blockchain.chain) < len(vs_blockchain.chain):
                            self.blockchain.from_json(json.dumps(jblockchain))

                    self.save_blockchain_to_file()

            except Exception:
                self.s.close()
                raise Exception

    def save_blockchain_to_file(self):
        f = open('blockchains/' + str(self.id) + '.txt', 'w+')
        f.write(self.blockchain.to_json())
        f.close()

    def sendmsg(self, s, jmsg):
        s.sendall(json.dumps(jmsg).encode())

    def recvmsg(self, s):
        data = ''
        while True:
            try:
                part = s.recv(2048)
                if not part: raise Exception()
                data += part.decode()
                data = json.loads(data)
            except json.JSONDecodeError:
                continue
            break

        return data

    def select(self, msg, opts, c = 0):
        while True:
            try:
                opt = int(input(msg))
                if opt in opts or c:
                    break
            except Exception:
                continue

        return opt

    def connect_network(self):
        try:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.connect(('localhost', 55123))
            self.s.sendall('#AABBCC'.encode())
            msg = self.s.recv(2048).decode()
            if (msg != 'ABCABC#'): return False
        except Exception:
            return False

        return True

    def connect_db(self):
        db = sl.connect('application.db')
        curs = db.cursor()
        curs.execute('select count(name) from sqlite_master where type= "table"')
        if curs.fetchone()[0] != 2:
            return False

        self.db = db
        self.curs = curs
        return True

    def print_info(self):
        print('Демонстрационная модель частного распределенного реестра.')
        print('Выполнена в рамках курсовой работы по дисциплине криптографические методы защиты информации.')
        print('Автор: Буренок Дмитрий Сергеевич. Руководитель: Бутакова Наталья Георгиевна.')
        print('НИУ МИЭТ, 2020 год, группа ИБ-31.\n')

if __name__ == '__main__':
    app = Application()
