import socket
import threading
import json
import glob
import sqlite3 as sl

class Network:
    def __init__(self):
        self.print_info()

        try:
            db = sl.connect('application.db')
            curs = db.cursor()
            curs.execute('select count(name) from sqlite_master where type= "table"')
            if curs.fetchone()[0] != 2:
                curs.execute('''create table users (
                                    id integer not null primary key autoincrement,
                                    role integer,
                                    publickey text,
                                    privatekey text
                               );
                ''');
        except sl.Error as err:
            print(err)
            print('Ошибка базы данных!')
            exit(2)


        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.bind(('localhost', 55123))
        self.s.listen(100)

        self.cl = []

        while True:
            c, addr = self.s.accept()
            threading.Thread(target=self.handler, args=(c, addr)).start()

        c.close()
        self.s.close()

    def handler(self, c, addr):
        data = c.recv(2048).decode()
        if data == '#AABBCC':
            c.sendall('ABCABC#'.encode())

            self.cl.append(c)
            print('Добавлен участник! Всего участников: ' + str(len(self.cl)))

            while True:
                try:
                    msg = self.recvmsg(c);
                    print(msg)
                    if msg['code'] == 'new_block':
                        self.broadcast(c, msg)
                    elif msg['code'] == 'req_blockchain':
                        self.sendmsg(c, self.res_blockchain())

                except:
                    self.remove(c)
                    break

    def res_blockchain(self):
        blockchains = []
        for path in glob.glob('blockchains/*.txt'):
            f = open(path, 'r')
            try:
                blockchains.append(json.load(f))
            except json.JSONDecodeError:
                continue
            f.close()

        return {'code':'res_blockchain', 'blockchains':blockchains}

    def sendmsg(self, c, jmsg):
        c.sendall(json.dumps(jmsg).encode())

    def recvmsg(self, c):
        data = ''
        while True:
            try:
                part = c.recv(2048)
                if not part: raise Exception()
                data += part.decode()
                data = json.loads(data)
            except json.JSONDecodeError:
                continue
            break

        return data

    def broadcast(self, c, msg):
        for cc in self.cl:
            if cc != c:
                try:
                    self.sendmsg(cc, msg)
                except:
                    self.remove(cc)

    def remove(self, c):
        if c in self.cl:
            self.cl.remove(c)
            c.close()
            print('Удален участник! Всего участников: ' + str(len(self.cl)))

    def print_info(self):
        print('Демонстрационная модель частного распределенного реестра.')
        print('Выполнена в рамках курсовой работы по дисциплине криптографические методы защиты информации.')
        print('Автор: Буренок Дмитрий Сергеевич. Руководитель: Бутакова Наталья Георгиевна.')
        print('НИУ МИЭТ, 2020 год, группа ИБ-31.\n')

if __name__ == '__main__':
    net = Network()
